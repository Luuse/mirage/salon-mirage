<div class="menu">
        <a href="#top">Salon Mirage</a>
        <a href="#editor" >Éditeurs / Publishers</a>
        <a href="#expo">Expositions / Exhibitions</a>
        <a href="#concert">Concerts</a>

</div>

<div class="contentInfo">
    <div id="colLeft">
      <br>
      <div class="Txt">
        <p>Mirage est un&nbsp;festival dédié à&nbsp;l’édition qui se&nbsp;tiendra les <span>15, 16 et&nbsp;17 septembre</span> à&nbsp;Bruxelles. Le&nbsp;festival combine un&nbsp;salon d'édition, trois expositions et&nbsp;des concerts tous les soirs.
        </p>
        <br>
        <p>Mirage is&nbsp;a&nbsp;comics and graphics festival to&nbsp;be held on&nbsp;<span>15, 16 and 17 september</span> at&nbsp;Brussels. The festival combine a&nbsp;fair with editors, three exhibitions and concerts every night.
        </p>
      </div>
    </div>
    <div class="Infos">
      <ul id="editor"><u>Éditeurs / Publishers</u><br><br>
        <h1>15 septembre <br> 18:00 ⤻ 23:00<br>16 et 17 septembre <br> 11:00 ⤻ 20:00</h1>
        <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
        <h4>
         <li><a href="https://animalpress.tumblr.com/">Animal Press</a></li>
         <li><a href="http://lecontrevent.tumblr.com/" target="_blank">Antoine Orand</a></li>
         <li><a href="http://www.oficina-arara.org/" target="_blank">Arara</a></li>
         <li><a href="http://www.autobahnprint.be/" target="_blank">Autobahn</a></li>
         <li><a href="https://www.facebook.com/bandedeedition/?fref=mentions" target="_blank">Bande 2</a></li>
         <li><a href="http://www.bichel.be/" target="_blank">Bichel éditions</a></li>
         <li><a href="http://blocbooks.tumblr.com/" target="_blank">Bloc Books</a></li>
         <li><a href="http://www.collectionrevue.com/" target="_blank">Collection revue</a></li>
         <li><a href="http://www.copiedouble.org/" target="_blank">Copie Double</a></li>
         <li><a href="http://g----------a.tumblr.com/" target="_blank">CSH</a></li>
         <li><a href="http://cuistax-cuistax.blogspot.be/" target="_blank">Cuistax</a></li>
         <li><a href="http://danslecieltoutvabien.tumblr.com/" target="_blank">DLCTVB</a></li>
         <li><a href="http://editionsabstraites.tumblr.com/" target="_blank">Editions Abstraites</a></li>
         <li><a href="http://editionsgitan.tumblr.com/" target="_blank">Éditions Gitan</a></li>
         <li><a href="http://editionspeinture.com/" target="_blank">Éditions Peinture</a></li>
         <li><a href="http://radiofranck.tumblr.com/" target="blank">Elliot Dadat</a></li>
         <li><a href="https://gigacomasciencegroup.tumblr.com/" target="_blank">Giga Coma Science Group</a></li>
         <li><a href="http://sugarlandparadise.tumblr.com/" target="_blank">Jeong Hwa Min</a></li>
         <li><a href="http://www.fremok.org/site.php?type=P&id=286" target="_blank">KnockOutsider komiks</a></li>
         <li><a href="http://www.lassewandschneider.de/" target="_blank">Lasse Wandschneider</a></li>
         <li><a href="https://linstitutserigraphique.tumblr.com/" target="_blank">L’institut Sérigraphique</a></li>
         <li><a href="http://www.lapoinconneuse.com/" target="_blank">La poinçonneuse</a></li>
         <li><a href="http://www.mauvaisefoi-editions.com/" target="_blank">mauvaise foi</a></li>
         <li><a href="https://www.matiere.org/" target="_blank">Matière</a></li>
         <li><a href="http://www.modelepuissance.com/" target="_blank">Modèle puissance</a></li>
         <li>MULTI/Tabbert 4500</li>
         <li><a href="http://nkzine.free.fr/" target="_blank">Nazi Knife</a></li>
         <li><a href="http://novland.bigcartel.com/" target="_blank">Novland</a></li>
         <li><a href="http://n-10.fr/" target="_blank">Numéro 10</a></li>
         <li><a href="http://papiermachine.fr" target="_blank">Papier machine</a></li>
         <li><a href="http://www.postindustrialanimism.net/" target="_blank">Post Industrial Animism</a></li>
         <li><a href="http://www.quentinchambry.com/" target="_blank">Quentin Chambry</a></li>
         <li><a href="http://www.romeojulien.fr/" target="_blank">Roméo Julien</a></li>
         <li><a href="https://www.facebook.com/rubierodareixira/" target="_blank">Rubiero da Reixirà</a></li>
         <li><a href="http://cargocollective.com/sammystein" target="_blank">Sammy Stein</a></li>
         <li><a href="https://www.shoboshobo.com/" target="_blank">Shoboshobo</a></li>
         <li><a href="http://stefanie-leinhos.de/" target="_blank">Stéphanie Leinhos</a></li>
         <li><a href="https://www.facebook.com/sprstrctr/?fref=ts" target="_blank">Super-Structure</a></li>
         <li><a href="http://surfaces-utiles.org/" target="_blank">Surfaces Utiles</a></li>
         <li><a href="http://www.terrybleu.com/" target="_blank">Terry bleu</a></li>
         <li><a href="http://toboggang.blogspot.be/" target="_blank">Tobboggan</a></li>
         <li><a href="http://jolidessin.blogspot.be/2013/08/tom-lebaron-kherif.html" target="_blank">Tom Lebaron Khérif</a></li>
         <li><a href="http://www.editions-tusitala.org/" target="_blank">Tusitala</a></li>
       </h4>
    </ul>
    <br><br>
     <ul id="expo"><u>Expositions / Exhibitions</u><br><br>
       <h1>
       Vernissage le 15 septembre à 18:00
      <br>15 septembre <br> 18:00 ⤻ 23:00
       <br>16 et 17 septembre <br> 11:00 ⤻ 20:00
       </h1>
       <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
       <h4>
       <li>ARARA / RÉTROSPECTIVE<br>
          <em>Arara est un collectif d’artistes basé a Porto depuis 2010, travaillant principalement à travers la sérigraphie et le workshop public. Dessins obscurs, photos et graphismes bruts encourageant une relecture de notre société, ils offrent avec leurs affiches géantes et leur livres un regard impactant et cynique sur le monde contemporain.
Avec les travaux de Miguel Carneiro, Dayana Lucas, Bruno Borges, João Avves, Pedro Nora & Luís Silva entre autres.          </em></li>
<br>
<li>AND NO MORE FRIENDS<em><br>
Alexander II,
<a href="http://cargocollective.com/antoineorand"  target="_blank" >Antoine Orand</a>,
<a href="https://www.instagram.com/haunted_horfee/" target="_blank" >Antwan Horfee</a>,
<a href="https://www.spraydaily.com/p/gues-pal-sdk-i-am-over-the-city/" target="_blank" >Guess</a>,
<a href="http://nkzine.free.fr/" target="_blank" >Hendrik Hegray</a>,
Jérôme Benitta,
<a href="http://www.dda-ra.org/fr/oeuvres/DELABORDE_Jonas" target="_blank" >Jonas Delaborde</a>,
<a href="http://www.leonsadler.com/" target="_blank" >Leon Sadler</a>,
<a href="https://www.instagram.com/picardomario/?hl=fr" target="_blank" >Mario Picardo</a>,
<a href=" https://www.instagram.com/sarahlouisebarbett/?hl=fr" target="_blank">Sarah Louise Barbett</a> & <a href="http://simon-john-thompson.com/" target="_blank" >Simon Thompson</a>.<br>
Commissariat d’Anne Lejemmetel et Manuel Morin.</em></li><br>
        <li>PREMIER RENCARD <em><br><a href="http://www.quentinchambry.com/"target="_blank">Quentin Chambry</a>,
          <a href="https://www.flickr.com/photos/51641802@N02/"target="_blank">Alexis Poline</a>,
          <a href="https://www.instagram.com/hard_style_doodle/"target="_blank">Adrien Frégosi
          <a href="https://www.instagram.com/hard_style_doodle/"target="blank"> Gsulf</a>  </em></li>
      </h4>
      </ul>
    <br><br>
     <ul id="concert"><u>Concerts</u><br><br>
       <div class="Date1">
         <h1>7 Septembre <br> 18:00 ⤻ 23:30 </h1>
         <h2>Niko Matcha <br> rue de Flandres 138 B, 1000 Bruxelles</h2>
         <h3>prix&thinsp;: 3€</h3>
         <h4>
           <li>Soirée de soutien du Salon
             Dj sets par le collectif <a href="http://poxcat.com/" target="_blank">Poxcat</a>:
           </li>
           <li>
             <a target="_blank">Dj Hyppocampo</a>
           </li>
           <li>
             <a target="_blank">Mika Mayonaise</a>
           </li>
           <li>
             <a target="_blank">+ Nurse +</a>
           </li>
           <li>
             <a target="_blank">Dj Amigo III</a>
           </li>


         </h4>
       </div>

      <br>
      <br>

       <div class="Date1">
        <h1>15 Septembre <br> 19:00 ⤻  23:30</h1>
        <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
        <h3>prix&thinsp;: 5€</h3>
        <h4>
          <li>
            <a target="_blank">Z.B Aids </a>
          </li>
          <li>
            <a href="https://soundcloud.com/pat-weld" target="_blank">Pat Weld</a>
          </li>
          <li>
            <a href="https://soundcloud.com/bearboneslaylow" target="_blank">Bear Bones Lay Low</a>
          </li>
          <li>
            <a target="_blank">dj set&thinsp;: Pavé de Bonnes Intentions</a>
          </li>
        </h4>
       </div>
      <br>
      <br>
       <div class="Date2">
         <h1>16 Septembre <br> 19:30 ⤻  23:30</h1>
         <h2>Hangar Communa<br>rue Gray 171, 1050 Ixelles</h2>
         <h3>prix&thinsp;: 5€</h3>
         <h4>
          <li>
            <a href="https://brunellobaptiste.bandcamp.com/releases" target="_blank">Baptiste Brunello</a>
          </li>
          <li>
            <a href="https://soundcloud.com/deejay_backstabber" target="_blank">Dj Backstabber</a>
          </li>
          <li>
            <a href="https://soundcloud.com/choolersdivision" target="_blank">The Choolers Division</a>
          </li>
         </h4>
       </div>
      <br>
      <br>
       <div class="Date3">
         <h1>17 Septembre <br> 19:30 ⤻ 23:30</h1>
         <h2>Niko Matcha<br>rue de Flandres 183B, 1000 Bruxelles</h2>
         <h3>prix&thinsp;: 5€</h3>
         <h4>
          <li>
            <a href="https://soundcloud.com/lapostepointnet" target="_blank">Accou</a>
          </li>
          <li>
            <a href="https://soundcloud.com/capeloband" target="_blank">Capelo</a>
          </li>
           <li>
             <a href="https://soundcloud.com/romeopoirier" target="_blank">Roméo Poirier</a>
           </li>
         </h4>
       </div>
      </ul>
      <!-- <img src="img/SOLEILNOIR.png" style="width: 15%; margin-top: 15px; opacity: 0.8;" alt="" /> -->
     </div>
   <div class="images">
     <img class="last img4" src="img/case_pyramidnoir.png" alt="">

   </div>
    <div class="credit">Design et développement&thinsp;: Luuse | typographie&thinsp;: <a href="http://lunchtype.com/" target="_blank" >Lunchtype</a></div>
</div>
