function getAndPushContent(){
  $.get('./ascii/mirage_basse-def.txt', function(data) {
    var rows = data.split('\n');
    for (var i = 0; i < rows.length; i++) {
      var cols = rows[i].split('');
      for (var j = 0; j < cols.length; j++) {
        if( cols[j] != ' ' ){
          $('.row'+i).children('.col'+j).html(cols[j]).addClass('fill');
        }else{
          $('.row'+i).children('.col'+j).html(cols[j]);
        }
      }
    }
  });

}

function mouseEvent(){
  var spanDiv = $('#content span');
  var palier = 1;
  $( "body" ).mousemove(function( event ) {

    var rot = parseInt(event.pageX);

    spanDiv.each(function(){

      if($(this).attr('class').split(' ')[2] == 'fill'){
        $(this).css('transform', 'rotate('+rot+'deg)');
        rot = rot + palier;
      }else {
        rot = rot + palier;
      }

    })
  });
}

function rotateOnScroll(scrollPos){

    var spanDiv = $('#content span');
    var palier = 1;
    var rot = scrollPos;

    spanDiv.each(function(){

      if($(this).attr('class').split(' ')[2] == 'fill'){
        $(this).css('transform', 'rotate('+rot+'deg)');
        rot = rot + palier;
      }else {
        rot = rot + palier;
      }

    })

}

function moveCol(scrollPos){
  // var col = $('#content .col');
  var num = scrollPos * 0.01;
  //
  // for (var i = 0; i < 4; i++) {
  //   var rand1 = Math.floor(Math.random() * (3 - 0) + 0);
  //   var rand2 = Math.floor(Math.random() * (118 - 0) + 0);
  //   $('.col'+rand2).css({'flex-grow': num * rand1});
  //
  // }

  $('.col20').css({'flex-grow': num});
  $('.col54').css({'flex-grow': num * 2});
  $('.col72').css({'flex-grow': num * 0.5});
  $('.col123').css({'flex-grow': num * 2.5});

  if (scrollPos <= 50) {
    $('.row3').css({'height': 'auto'});
    $('.row10').css({'height': 'auto'});
    $('.row14').css({'height': 'auto'});
    $('.row17').css({'height': 'auto'});
    $('.row19').css({'height': 'auto'});
  } else{
    $('.row3').css({'height': num * 1.5});
    $('.row10').css({'height': num * 7});
    $('.row14').css({'height': num * 3});
    $('.row17').css({'height': num * 1});
    $('.row19').css({'height': num * 4.5});
  }

}

function showImages(images, colLeftH, colRightH, scrollPos){
    if ($(window).width() >= 800) {
      if (scrollPos >= colLeftH) {
        images.children('.img4').show();
      } else {
        images.children('.img4').hide();
      }
    } else {
      images.children('.img4').show();
    }
    // if (scrollPos >= colLeftH && scrollPos < colLeftH + 200) {
    //   images.children('img').hide();
    //   images.children('.img1').show();
    //   imagesH(images.children('.img1'));
    // } else if (scrollPos >= colLeftH + 200 && scrollPos < colLeftH + 400) {
    //   images.children('img').hide();
    //   images.children('.img2').show();
    //   imagesH(images.children('.img2'));
    //
    // } else if (scrollPos >= colLeftH + 400 && scrollPos < colLeftH + 600) {
    //   images.children('img').hide();
    //   images.children('.img3').show();
    //   imagesH(images.children('.img3'));
    //
    // } else if (scrollPos >= colRightH) {
    //   images.children('img').hide();
    //   images.children('.img4').show();
    //   imagesH(images.children('.img4'));
    //
    // } else {
    //   images.children('img').hide();
    // }


}


function showImagesMobile(scrollPos){
  var images = $('.images');
  var contentH = $('.contentInfo').outerHeight();
    if (scrollPos >= contentH && scrollPos < contentH + 300) {
      images.children('.img4').show();
    } else {
      images.children('img').hide();
    }
    // if (scrollPos >= contentH && scrollPos < contentH + 200) {
    //   images.children('img').hide();
    //   images.children('.img1').show();
    //   imagesH(images.children('.img1'));
    // } else if (scrollPos >= contentH + 200 && scrollPos < contentH + 400) {
    //   images.children('img').hide();
    //   images.children('.img2').show();
    //   imagesH(images.children('.img2'));
    //
    // } else if (scrollPos >= contentH + 400 && scrollPos < contentH + 600) {
    //   images.children('img').hide();
    //   images.children('.img3').show();
    //   imagesH(images.children('.img3'));
    //
    // } else if (scrollPos >= colRightH + 800) {
    //   images.children('img').hide();
    //   images.children('.img4').show();
    //   imagesH(images.children('.img4'));
    //
    // } else {
    //   images.children('img').hide();
    // }

}

function decale(scrollPos){

  var row = $('#content .row');
  var i = scrollPos * 0.05;
  // console.log(i);
  row.each(function(){
    var random = Math.random() * (i - 0) + 0;
      $(this).css({'padding-left': i+random});
      $(this).css({'margin-left': (-(i +random))/2});
  })

}

function txtMargin(width){
    $('.contentInfo').css({'margin-top': width/4});
}
