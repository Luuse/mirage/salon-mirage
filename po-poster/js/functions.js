var idLetter = 0;

function getAndPushContent(letter, outputZone){
  var letterFile = letter+'.txt';
  $.get('./ascii/'+letterFile, function(data) {
    var rows = data.split('\n');
    var numRow = rows.length;
    var numCol = 38;
    var widthSpan = 10;
    var name = letterFile.split('.');

    // A D D   C A D R A T I N
    $('<div />', {
      id: name[0]+'_'+idLetter,
      class: 'cadratin'
    }).appendTo(outputZone);

    // A D D   R O W -> C A D R A T I N
    for (var r = 0; r < numRow; r++) {
      $('<div/>', {
        id: name[0]+'_'+idLetter+'_r_'+r,
        class: 'row',
      }).appendTo('#'+name[0]+'_'+idLetter);

      var cols = rows[r].split('');
      console.log(cols[1])
      for (var j = 0; j < cols.length; j++) {
        if( cols[j] != ' ' ){
          $('<span/>', {
            class: 'col fill',
            text: cols[j]

          }).appendTo('#'+name[0]+'_'+idLetter+'_r_'+r);
          // $('#'+name[0]+' .row').children('.col'+j).html(cols[j]).addClass('fill');
        }else{
          $('<span/>', {
            class: 'col',
            text: ' '

          }).appendTo('#'+name[0]+'_'+idLetter+'_r_'+r);
        }
      }
    }
    idLetter++;
  });
}

function mouseEvent(){
  var spanDiv = $('span');
  var palier = 20;
  $( "body" ).mousemove(function( event ) {

    var rot = parseInt(event.pageX);

    spanDiv.each(function(){

      if($(this).attr('class').split(' ')[2] == 'fill'){
        $(this).css('transform', 'rotate('+rot+'deg)');
        rot = rot + palier;
      }else {
        rot = rot + palier;
      }

    })
  });
}


$("body").keypress( function( event ) {
  console.log(event.key);
  getAndPushContent(event.key, '#contentOne');
  mouseEvent();
})



function rotateOnScroll(scrollPos){

    var spanDiv = $('.cadratin span');
    var palier = 1;
    var rot = scrollPos;

    spanDiv.each(function(){

      if($(this).attr('class').split(' ')[2] == 'fill'){
        $(this).css('transform', 'rotate('+rot+'deg)');
        rot = rot + palier;
      }else {
        rot = rot + palier;
      }

    })

}

function moveCol(scrollPos){
  // var col = $('#content .col');
  var num = scrollPos * 0.01;
  //
  // for (var i = 0; i < 4; i++) {
  //   var rand1 = Math.floor(Math.random() * (3 - 0) + 0);
  //   var rand2 = Math.floor(Math.random() * (118 - 0) + 0);
  //   $('.col'+rand2).css({'flex-grow': num * rand1});
  //
  // }

  $('.col20').css({'flex-grow': num});
  $('.col54').css({'flex-grow': num * 2});
  $('.col72').css({'flex-grow': num * 0.5});
  $('.col123').css({'flex-grow': num * 2.5});

  if (scrollPos <= 50) {
    $('.row3').css({'height': 'auto'});
    $('.row10').css({'height': 'auto'});
    $('.row14').css({'height': 'auto'});
    $('.row17').css({'height': 'auto'});
    $('.row19').css({'height': 'auto'});
  } else{
    $('.row3').css({'height': num * 1.5});
    $('.row10').css({'height': num * 7});
    $('.row14').css({'height': num * 3});
    $('.row17').css({'height': num * 1});
    $('.row19').css({'height': num * 4.5});
  }

}


function onInputSimple(chemin, set){
  var word = $('.titre').html();
  var nb_car = word.length;
  var letter = word.split("");
  $('.titre').html('');
    for (var i = 0, l = word.length; i < l; i++) {
      var v = letter[i];

      getAndPushContent(v, '#contentOne');
    }
}

onInputSimple( 'condensed', 'input' );


//
// function showImagesMobile(scrollPos){
//   var images = $('.images');
//   var contentH = $('.contentInfo').outerHeight();
//     if (scrollPos >= contentH && scrollPos < contentH + 300) {
//       images.children('.img4').show();
//     } else {
//       images.children('img').hide();
//     }
// }
//
// function decale(scrollPos){
//
//   var row = $('#content .row');
//   var i = scrollPos * 0.05;
//   // console.log(i);
//   row.each(function(){
//     var random = Math.random() * (i - 0) + 0;
//       $(this).css({'padding-left': i+random});
//       $(this).css({'margin-left': (-(i +random))/2});
//   })
//
// }
//
// function txtMargin(width){
//     $('.contentInfo').css({'margin-top': width/4});
// }
