import fontforge
import sys

font = sys.argv[1]
name = sys.argv[2]

for gly in fontforge.open(font).glyphs():
	gly.export("output/"+name+"/" +gly.glyphname + ".bmp", 10)

